class Dices:
    def __init__(self,N,M):
        self.first = int(N)
        self.second = int(M)

def printANS (str):
    print str
    return


with open('read.txt') as fp:
    f = lambda x,j: printANS (x+j)
    for lin in fp:
        Dice = Dices(lin.split()[0],lin.split()[1])
        if Dice.first == Dice.second:
            printANS (Dice.first + 1)
            print "\n"
        elif Dice.first<Dice.second:
            z = abs(Dice.first-Dice.second)+1
            for i in range(1,z+1):
                f(Dice.first,i)
            print "\n"
        elif Dice.first>Dice.second:
            z = abs(Dice.first-Dice.second)+1
            for i in range(1,z+1):
                f(Dice.second,i) 
            print "\n"
        

